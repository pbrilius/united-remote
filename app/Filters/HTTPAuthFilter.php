<?php namespace App\Filters;

use App\Libraries\Auth;
use App\Models\User;
use CodeIgniter\Filters\FilterInterface;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Session\Session;

class HTTPAuthFilter implements FilterInterface
{
    public function before(RequestInterface $request)
    {
        // dd('test');
        /**
         * @var Auth
         */
        $auth = service('auth');

        if ($auth->isLoggedIn()) {
            return $request;
        }

        /**
         * @var URI
         */
        $uri = $request->uri;

        $userInfo = $uri->getUserInfo();

        if (is_null($userInfo)) {
            return $request;
        }
        // d($userInfo);
        $credentials = explode(':', $userInfo);

        // dd($credentials);
        if (sizeof($credentials) === 0) {
            return $request;
        }

        /**
         * @var Builder
         */
        $builder = (new User())->builder();
        $user = $builder
            ->where([
                'email' => $credentials[0],
                'hash' => hash('whirlpool', $credentials[1])
            ])
            ->findOne();

        /**
         * @var Session
         */
        $session = session();
        
        if (sizeof($user) === 0) {
            $session->setFlashdata('error', 'Not logged in');
            return $request;
        }


        $session->set([
            'userEmail' => $user->getEmail(),
            'userRole' => $user->getLabel(),
            'loggedIn' => true,
        ]);
    }

    public function after(RequestInterface $request, ResponseInterface $response)
    {
        /**
         * @var Auth
         */
        $auth = service('auth');

        if (!$auth->isLoggedIn()) {
            $response->setHeader('WWW-Authenticate', 'Basic realm="Poker game"');
            $response->setStatusCode(401);
        }
    }
}
