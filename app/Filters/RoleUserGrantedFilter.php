<?php namespace App\Filters;

use App\Libraries\Auth;
use App\Models\Role;
use CodeIgniter\Filters\FilterInterface;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Session\Session;

class RoleUserGrantedFilter implements FilterInterface
{
    public function before(RequestInterface $request)
    {
        /**
         * @var Auth
         */
        $auth = service('auth');
        /**
          * @var Session
          */
        $session = session(); 

        if (!$auth->isLoggedIn()) {
            $session->setFlashdata('error', 'Not logged in');
            return $request;
        }

        $roleModel = new Role();

        $builder = $roleModel->builder();
        $userRole = $builder
            ->where('label', env('role.user'))
            ->findOne();

        if (!$auth->posessesRoleOf($userRole)) {
            $session->setFlashdata('error', 'Not a user');
        }
        
        return $request;
    }

    public function after(RequestInterface $request, ResponseInterface $response)
    {
        /**
         * @var Session
         */
        $session = session();
        if ($session->getFlashdata('error') === 'Not a user' || $session->getFlashdata('error' === 'Not logged in')) {
            $response = $response->setHeader('WWW-Authenticate', 'Basic realm="Poker game"');
            $response = $response->setStatusCode(401);
        }

        return $response;
    }
}