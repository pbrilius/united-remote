<?php namespace App\Models;

use CodeIgniter\Model;

class User extends Model
{
    /**
     * Database table
     *
     * @var string
     */
    protected $table = 'user';

    /**
     * Allowed fields
     *
     * @var array
     */
    protected $allowedFields = [
        'email',
        'hash',
        'roleId',
    ];

    protected $useTimestamps = true;

    protected $returnType = 'array';
}