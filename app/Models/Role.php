<?php namespace App\Models;

use CodeIgniter\Model;

class Role extends Model
{
    /**
     * Database table
     *
     * @var string
     */
    protected $table = 'role';

    protected $allowedFields = [
        'label',
        'parentRole',
    ];

    protected $useTimestamps = true;

    protected $returnType = 'object';
}