<?php namespace App\Models;

use App\Entities\Hands as EntitiesHands;
use CodeIgniter\Model;

class Hands extends Model
{
    /**
     * Database table
     *
     * @var string
     */
    protected $table = 'hands';

    protected $allowedFields = [
        'cards',
    ];

    protected $useTimestamps = true;

    protected $returnType = EntitiesHands::class;
    
}
