<?php namespace App\Libraries;

use App\Models\Role;

class Auth
{

    public function isLoggedIn()
    {
        $session = session();

        return $session->get('loggedIn');
    }

    public function posessesRoleOf(Role $role)
    {
        $session = session();

        return $session->get('userRole') === $role->getLabel();
    }
}