<?php namespace App\Controllers\Tools;

use App\Controllers\BaseController;
use App\Models\User as ModelsUser;
use CodeIgniter\CLI\CLI;
use CodeIgniter\Controller;
use CodeIgniter\Database\MySQLi\Builder;
use Tests\Support\Models\UserModel;

class User extends Controller
{
    /**
     * User add command
     *
     * @param string $email
     * @param string $password
     * @return void
     */
    public function add(string $email, string $password)
    {
        // $email = $argv[1];
        // $password = $argv[2];

        dd($email);

        if (strlen($email) < 5 || strlen($password) < 5) {
            throw new \Exception('User email and/or password must be provided');
        }

        CLI::write('Adding to database');

        $userModel = new ModelsUser();
        
        /**
         * @var Builder
         */
        $builder = $userModel->builder();
        $builder->where([
            'email' => $email,
        ])
            ->delete();

        $userModel
            ->setEmail($email)
            ->setHash(hash('whirlpool', $password))
            ->save();

        CLI::write('User saved', 'green');
    }

    public function delete(string $email)
    {
        dd($email);
        if (strlen($email) < 5) {
            throw new \Exception('User email must be provided');
        }


        /**
         * @var Builder
         */
        $builder = (new UserModel())->builder;
        $builder
            ->where([
                'email' => $email,
            ])
            ->delete();

        CLI::write('User deleted', 'light_red');
    }
}