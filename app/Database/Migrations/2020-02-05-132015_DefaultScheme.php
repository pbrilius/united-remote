<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class DefaultScheme extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id'          => [
				'type'           => 'INT',
				'constraint'     => 5,
				'unsigned'       => TRUE,
				'auto_increment' => TRUE
			],
			'cards'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '512',
			],
		]);
		$this->forge->addKey('id', TRUE);
		$this->forge->createTable('hands');
	}

	public function down()
	{
		$this->forge->dropTable('hands');
	}
}
