<?php namespace App\Entities;

use CodeIgniter\Entity;

class Hands extends Entity
{
    protected $casts = [
        'cards' => 'json-array',
    ];
}
