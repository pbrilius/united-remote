<?php namespace App\Commands;

use App\Entities\Hands;
use App\Models\Hands as ModelsHands;
use CodeIgniter\CLI\BaseCommand;
use CodeIgniter\CLI\CLI;

class FileUploadCommand extends BaseCommand
{
    protected $group = 'Backend';

    protected $name = 'app:upload';

    protected $description = 'Upload poker hands combinations to database';

    protected $_lineLength = 31;

    /**
     * Command execution
     *
     * @param array $params
     * @return void
     */
    public function run(array $params)
    {
        $fileLocation = ROOTPATH . '/' .  env('hand.combinations');
        CLI::write('File location: ' . CLI::color($fileLocation, 'yellow'), 'white');

        if (!file_exists($fileLocation)) {
            CLI::error('File doesn\'t exist, upload it to server.');
        }

        $fileHandle = fopen($fileLocation, 'r');
        $handsUploaded = [];
        while (!feof($fileHandle)) {
            $handsUploaded []= [
                'cards' => trim(fgets($fileHandle, $this->_lineLength)),
            ];
        }

        fclose($fileHandle);
        
        $handsUploaded = array_filter($handsUploaded);
        $progressSteps = sizeof($handsUploaded);
        $handsModel = new ModelsHands();

        for ($i = 0; $i < $progressSteps; $i++) {
            $hands = new Hands($handsUploaded[$i]);
            $handsModel->save($hands);
            CLI::showProgress($i, $progressSteps);
        }

        CLI::write('Uploaded', 'green');
    }
}
