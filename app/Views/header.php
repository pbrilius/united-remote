<div class="jumbotron">
	<h1 class="display-4"><?= $title ?></h1>
	<p class="lead">Poker hands 2 players winner app - <a href="https://unitedremote.com/" target="_blank">United remote</a> test task solution.</p>
	<hr class="my-4">
	<p>It's based on Codeigniter with webpack, bootstrap and layered views dashboard.</p>
	<a class="btn btn-primary btn-lg" href="<?= site_url('poker/hands-winner') ?>" role="button">Poker game winner</a>
</div>
