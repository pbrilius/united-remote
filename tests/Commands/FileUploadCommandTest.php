<?php namespace App\Commands;

use CIUnitTestCase;

class FileUploadCommandTest extends CIUnitTestCase
{

    public function setUp(): void
    {
        parent::setUp();
    }

    public function testInvocation()
    {
        $fileUploadCommandMock = $this
            ->getMockBuilder(FileUploadCommand::class)
            ->disableOriginalConstructor()
            ->getMock()
            ;
        
        $this->assertEquals(31, $this->getPrivateProperty($fileUploadCommandMock, '_lineLength'));
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }
}