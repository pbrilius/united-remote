<?php namespace Tests\Support\Database;

use CodeIgniter\Database\Seeder;

class HandsSeed extends Seeder
{
    private static $upperLimit = 1000;

    public function run()
    {
        for ($i = 0; $i < static::$upperLimit; $i++) {
            $cards = [];
            for ($j = 0; $j < 10; $j++) {
                $cards []= rand(1, 10) . substr(uniqid(), 0, 1);
            }
            $data = [
                'cards' => $cards,
            ];
            $this->db->table('hands')->insert($data);
        }
    }
}