<?php namespace App\Models;

use App\Entities\Hands as EntitiesHands;
use CIUnitTestCase;

class HandsTest extends CIUnitTestCase
{
    public function testModelProperties()
    {
        $handsModelMock = $this->getMockBuilder(Hands::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->assertEquals('hands', $this->getPrivateProperty($handsModelMock, 'table'));
        $this->assertArraySubset(['cards'], $this->getPrivateProperty($handsModelMock, 'allowedFields'));
        $this->assertTrue($this->getPrivateProperty($handsModelMock, 'useTimestamps'));
        $this->assertEquals($this->getPrivateProperty($handsModelMock, 'returnType'), EntitiesHands::class);
    }
}